# Contributors

These are the people responsable for what Mythic Table is today:

* Marc Faulise
* Victor Oliveira
* Adam Sturge
* Jon Winsley
* Mirko Rainer (GRK)
* Keith Valin
* James Hatheway
* Nikita Golev
* Paul Sheean
* Ankit Sangwan
* Ben Milman
* ...
