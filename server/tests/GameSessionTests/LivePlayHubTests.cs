using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Moq;
using MythicTable;
using MythicTable.Campaign.Data;
using MythicTable.Campaign.Util;
using MythicTable.GameSession;
using Newtonsoft.Json.Linq;
using Xunit;

namespace LivePlayTests
{
    // TODO #6: Test GameState
    public class LivePlayHubTests
    {
        public Mock<IEntityCollection> stateMock;
        public Mock<IGameState> gameStateMock;
        public Mock<ICampaignProvider> campaignProviderMock;
        public Mock<ILogger<LivePlayHub>> loggerMock;
        public Mock<IHubCallerClients<ILiveClient>> clientsMock;
        public Mock<ILiveClient> allClientsMock;

        public LivePlayHub hub;

        public LivePlayHubTests()
        {
            stateMock = new Mock<IEntityCollection>();
            gameStateMock = new Mock<IGameState>();
            campaignProviderMock = new Mock<ICampaignProvider>();
            loggerMock = new Mock<ILogger<LivePlayHub>>();
            clientsMock = new Mock<IHubCallerClients<ILiveClient>>();
            allClientsMock = new Mock<ILiveClient>();
            
            clientsMock.Setup(m => m.All).Returns(allClientsMock.Object);
            allClientsMock
                .Setup(a => a.ConfirmDelta(It.IsAny<SessionDelta>()))
                .Returns(Task.CompletedTask);
            allClientsMock
                .Setup(a => a.ReceiveDiceResult(It.IsAny<RollDTO>()))
                .Returns(Task.CompletedTask);
            allClientsMock
                .Setup(a => a.CharacterAdded(It.IsAny<CharacterDTO>()))
                .Returns(Task.CompletedTask);

            hub = new LivePlayHub(stateMock.Object, gameStateMock.Object, campaignProviderMock.Object, loggerMock.Object);
            
            hub.Clients = clientsMock.Object;
            hub.Context = Mock.Of<HubCallerContext>();
        }

        [Fact]
        public async Task GameStepIsSubmittedToStateManagerOnce()
        {
            stateMock
                .Setup(s => s.ApplyDelta(It.IsAny<IEnumerable<EntityOperation>>()))
                .Returns(Task.FromResult(true));

            clientsMock.Setup(m => m.All).Returns(Mock.Of<ILiveClient>());
            hub.Clients = clientsMock.Object;

            var submittedStep = new SessionDelta
            {
                Entities = new[]
                {
                    new EntityOperation
                    {
                        EntityId = "obj1",
                        Patch = new JsonPatchDocument().Add("/a/b/c", "foo"),
                    },
                },
            };

            await hub.UpdateCharacter(submittedStep);

            stateMock.Verify(
                entities => entities.ApplyDelta(
                    It.Is<IEnumerable<EntityOperation>>(step => step == submittedStep.Entities)),
                Times.Once);
        }

        [Fact]
        public async Task GameStepIsRepeatedToAllClientsOnce()
        {
            stateMock
                .Setup(s => s.ApplyDelta(It.IsAny<IEnumerable<EntityOperation>>()))
                .Returns(Task.FromResult(true));

            var submittedStep = new SessionDelta
            {
                Entities = new[]
                {
                    new EntityOperation
                    {
                        EntityId = "obj1",
                        Patch = new JsonPatchDocument().Add("/a/b/c", "foo"),
                    },
                },
            };

            await hub.UpdateCharacter(submittedStep);

            allClientsMock.Verify(
                c => c.ConfirmDelta(It.Is<SessionDelta>(step => step == submittedStep)),
                Times.Once());
        }

        [Fact]
        public async Task FailedSubmissionsAreNotRepeated()
        {
            stateMock
                .Setup(s => s.ApplyDelta(It.IsAny<IEnumerable<EntityOperation>>()))
                .Returns(Task.FromResult(false));

            var submittedStep = new SessionDelta
            {
                Entities = new[]
                {
                    new EntityOperation
                    {
                        EntityId = "obj1",
                        Patch = new JsonPatchDocument().Add("/a/b/c", "foo"),
                    },
                },
            };

            await hub.UpdateCharacter(submittedStep);

            allClientsMock.Verify(
                c => c.ConfirmDelta(It.Is<SessionDelta>(step => step == submittedStep)),
                Times.Never);
        }

        [Fact]
        public async Task ValidDiceRollsAreExecuted()
        {
            var roll = new RollDTO
            {
                ClientId = "123",
                SessionId = "test",
                Timestamp = 5678,
                Formula = "1d5"
            };

            await hub.RollDice(roll);

            allClientsMock.Verify(
                c => c.ReceiveDiceResult(It.Is<RollDTO>(rolled => rolled.Result != "Invalid dice roll" && rolled.ClientId == "123" && rolled.SessionId == "test" && rolled.Timestamp == 5678)),
                Times.Once());
        }

        [Fact]
        public async Task InvalidDiceRollsAreNotExecuted()
        {
            var callerMock = new Mock<ILiveClient>();
            clientsMock.Setup(m => m.Caller).Returns(callerMock.Object);

            callerMock
                .Setup(a => a.ExceptionRaised(It.IsAny<string>()))
                .Returns(Task.CompletedTask);
            hub.Clients = clientsMock.Object;
            hub.Context = Mock.Of<HubCallerContext>();

            var roll = new RollDTO
            {
                ClientId = "123",
                SessionId = "test",
                Timestamp = 5678,
                Formula = "aaaaaaaaa"
            };

            await hub.RollDice(roll);

            callerMock.Verify(
                c => c.ExceptionRaised(It.Is<string>(s => s.Contains("Invalid dice roll"))),
                Times.Once());
        }

        [Fact]
        public async Task MovesCharacter()
        {
            campaignProviderMock
                .Setup(cp => cp.UpdateCharacter(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<JsonPatchDocument>()))
                .Returns(Task.FromResult(1L));
            
            var submittedStep = new SessionDelta
            {
                CampaignId = "test-campaign",
                Entities = new[]
                {
                    new EntityOperation
                    {
                        EntityId = "character1",
                        Patch = new JsonPatchDocument().Add("/token/pos", JObject.Parse("{'q': 1, 'r': 2}")),
                    },
                },
            };

            await hub.UpdateCharacter(submittedStep);

            campaignProviderMock.Verify(
                c => c.UpdateCharacter(
                    It.Is<string>(campaignId => campaignId == submittedStep.CampaignId),
                    It.Is<string>(id => id == "character1"),
                    It.IsAny<JsonPatchDocument>()),
                Times.Once);
        }

        [Fact]
        public async Task AddCharacter()
        {
            var request = new AddCharacterRequest
            {
                campaignId = "test-campaign",
                x = 1,
                y = 2,
                image = "https://example.com/character.png"
            };
            var character = CharacterUtil.CreateCharacter(request.image, request.x, request.y);

            campaignProviderMock
                .Setup(cp => cp.AddCharacter(It.IsAny<string>(), It.IsAny<CharacterDTO>()))
                .Returns(Task.FromResult(character));

            await hub.AddCharacter(request);

            campaignProviderMock.Verify(
                c => c.AddCharacter(
                    It.Is<string>(campaignId => campaignId == request.campaignId),
                    It.Is<CharacterDTO>(dto =>
                        dto.Token["pos"]["q"] == request.x &&
                        dto.Token["pos"]["r"] == request.y &&
                        dto.Asset["src"] == request.image
                    )),
                Times.Once);

            allClientsMock.Verify(
                c => c.CharacterAdded(
                    It.Is<CharacterDTO>(dto =>
                        dto.Token["pos"]["q"] == request.x &&
                        dto.Token["pos"]["r"] == request.y &&
                        dto.Asset["src"] == request.image
                    )),
                Times.Once);
        }

        [Fact]
        public async Task RemoveCharacterTest()
        {
            campaignProviderMock.Setup(cp => cp.RemoveCharacter(It.IsAny<string>(), It.IsAny<string>()));
            
            var request = new RemoveCharacterRequest
            {
                CampaignId = "test-campaign",
                CharacterId = "test-character",
            };

            await hub.RemoveCharacter(request);

            campaignProviderMock.Verify(
                c => c.RemoveCharacter(
                    It.Is<string>(campaignId => campaignId == request.CampaignId),
                    It.Is<string>(characterId => characterId == request.CharacterId)),
                Times.Once);
        }
    }
}
