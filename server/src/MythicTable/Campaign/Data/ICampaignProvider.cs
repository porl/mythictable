using Microsoft.AspNetCore.JsonPatch;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MythicTable.Campaign.Data
{
    public interface ICampaignProvider
    {
        Task<List<CampaignDTO>> GetAll();
        Task<CampaignDTO> Get(string campaignId);
        Task<CampaignDTO> Create(CampaignDTO campaign, PlayerDTO owner);
        Task<CampaignDTO> Update(CampaignDTO campaign);
        Task Delete(string id);
        Task<List<PlayerDTO>> GetPlayers(string campaignId);
        Task<CampaignDTO> AddPlayer(string campaignId, PlayerDTO player);
        Task<CampaignDTO> RemovePlayer(string campaignId, PlayerDTO player);
        
        Task<List<CharacterDTO>> GetCharacters(string campaignId);
        Task<CharacterDTO> AddCharacter(string campaignId, CharacterDTO character);
        Task<List<CharacterDTO>> AddCharacters(string campaignId, List<CharacterDTO> characters);
        Task RemoveCharacter(string campaignId, string characterId);
        Task<long> MoveCharacter(string campaignId, string characterId, double x, double y);
        Task<long> UpdateCharacter(string campaignId, string characterId, JsonPatchDocument patch);
        
        Task<List<RollDTO>> GetRolls(string campaignId);
        Task<RollDTO> AddRoll(string campaignId, RollDTO roll);
    }
}