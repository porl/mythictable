namespace MythicTable.Campaign.Exceptions
{
        public class CampaignRemovePlayerException : CampaignException
        {
            public CampaignRemovePlayerException(string msg): base(msg) {}
        }
}