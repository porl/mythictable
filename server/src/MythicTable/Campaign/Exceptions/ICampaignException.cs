﻿using System.Net;

namespace MythicTable.Campaign.Exceptions
{
    public interface ICampaignException
    {
        /// <summary>
        /// HTTP status code linked to this kind of exception.
        /// </summary>
        public HttpStatusCode StatusCode { get; }
    }
}
