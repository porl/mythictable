using System;
using System.Net;

namespace MythicTable.Campaign.Exceptions
{
    public class CampaignException : Exception, ICampaignException
    {
        public CampaignException(string msg) : base(msg) { }

        public virtual HttpStatusCode StatusCode => HttpStatusCode.BadRequest;
    }
}