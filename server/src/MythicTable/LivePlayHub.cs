﻿using System.Threading.Tasks;
using System.Security.Claims;
using System.Linq;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using MythicTable.Campaign.Data;
using MythicTable.Campaign.Util;
using MythicTable.GameSession;
using Newtonsoft.Json.Linq;
using Dice;
using System;

namespace MythicTable
{
    public class LivePlayHub : Hub<ILiveClient>
    {
        public IEntityCollection EntityCollection { get; }
        public IGameState GameState { get; }
        public ICampaignProvider CampaignProvider { get; }
        private readonly ILogger logger;

        public LivePlayHub(IEntityCollection entities, IGameState gamestate, ICampaignProvider campaignProvider, ILogger<LivePlayHub> logger)
        {
            EntityCollection = entities;
            GameState = gamestate;
            CampaignProvider = campaignProvider;
            this.logger = logger;
        }

        // TODO: #17: A difference between the GameState on the server and the GameState for player clients exists. This means a delta should not just be broadcast once applied
        // The GM client will resolve actions and submit 1 delta for the players and another delta for the server
        [Authorize]
        [HubMethodName("submitDelta")]
        public async Task<bool> UpdateCharacter(SessionDelta delta)
        {
            // TODO #6: This should call the GameState ApplyDelta. EntityCollection functionality should be ported and generalised.
            var success = await EntityCollection.ApplyDelta(delta.Entities);

            await InternalUpdateCharacter(delta);
            
            if (success)
            {
                await Clients.All.ConfirmDelta(delta);
            }

            return success;
        }

        [Authorize]
        public async Task<CharacterDTO> AddCharacter(AddCharacterRequest request)
        {
            var character = CharacterUtil.CreateCharacter(request.image, request.x, request.y);
            character = await CampaignProvider.AddCharacter(request.campaignId, character);
            await Clients.All.CharacterAdded(character);
            return character;
        }

        [Authorize]
        public async Task<bool> RemoveCharacter(RemoveCharacterRequest request)
        {
            await CampaignProvider.RemoveCharacter(request.CampaignId, request.CharacterId);
            await Clients.All.CharacterRemoved(request.CharacterId);
            return true;
        }

        [Authorize]
        [HubMethodName("rollDice")]
        public async Task<bool> RollDice(RollDTO roll)
        {
            try
            {
                RollResult result = Roller.Roll(roll.Formula);
                roll.Result = result.ToString();
                this.logger.LogInformation($"Dice Roll - User: {roll.UserId} Roll: {roll.Formula} Results: {roll.Result}");
                var campaignId = roll.SessionId;
                await CampaignProvider.AddRoll(campaignId, roll);
                await Clients.All.ReceiveDiceResult(roll);
            }
            catch (DiceException)
            {
                string exception = "Invalid dice roll";
                await Clients.Caller.ExceptionRaised(exception);
            }

            return true;
        }

        [Authorize]
        [HubMethodName("submitDeltaTemp")]
        public async Task<bool> RebroadcastDeltaTemp(SessionOpDelta delta)
        {
            // TODO #6: This should call the GameState ApplyDelta. EntityCollection functionality should be ported and generalised.
            var success = await GameState.ApplyDelta(delta);

            if (success)
            {
                await Clients.All.ConfirmOpDelta(delta);
            }

            return success;
        }

        [Authorize]
        [HubMethodName("submitUndo")]
        public async Task<bool> RebroadcastUndo(string undoPlaceHolder)
        {
            var success = await GameState.Undo(undoPlaceHolder);

            if (success)
            {
                await Clients.All.Undo();
            }

            return success;
        }

        [Authorize]
        [HubMethodName("submitRedo")]
        public async Task<bool> RebroadcastRedo(string redoPlaceholder)
        {
            var success = await this.GameState.Redo(redoPlaceholder);

            if (success)
            {
                await Clients.All.Redo();
            }

            return success;
        }

        private async Task<bool> InternalUpdateCharacter(SessionDelta delta)
        {
            var campaignId = delta.CampaignId;
            foreach(var entity in delta.Entities)
            {
                this.logger.LogInformation($"Entity: {entity.EntityId}");
                await this.CampaignProvider.UpdateCharacter(campaignId, entity.EntityId, entity.Patch);
            }
            return true;
        }
    }
}
