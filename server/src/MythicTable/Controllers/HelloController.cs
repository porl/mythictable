using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MythicTable.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HelloController : ControllerBase
    {
        // GET: api/hello
        [HttpGet]
        public string Hello()
        {
            return "hello";
        }

        // GET: api/hello/me
        [HttpGet("me")]
        [Authorize]
        public string HelloMe()
        {
            return "hello "+ HttpContext.User.Identity.Name;
        }
    }
}
