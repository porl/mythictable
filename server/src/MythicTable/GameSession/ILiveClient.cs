﻿using System.Threading.Tasks;
using MythicTable.Campaign.Data;

namespace MythicTable.GameSession
{
    public interface ILiveClient
    {
        Task ConfirmDelta(SessionDelta delta);
        
        Task CharacterAdded(CharacterDTO character);
        
        Task CharacterRemoved(string characterId);

        Task ExceptionRaised(string exception);

        //TODO #6: Change the SessionDelta to be the SessionOpDelta
        Task ConfirmOpDelta(SessionOpDelta delta);

        Task Undo();

        Task Redo();

        Task ReceiveDiceResult(RollDTO roll);
    }
}
