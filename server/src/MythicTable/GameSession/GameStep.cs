﻿using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using Newtonsoft.Json;

namespace MythicTable.GameSession
{
    public class SessionDelta
    {
        [JsonProperty("campaignId")]
        public string CampaignId { get; set; }

        [JsonProperty("entities")]
        public IEnumerable<EntityOperation> Entities { get; set; }
    }

    // TODO #6: Object of this class represent deltas applied to the gamestate. They exist as a list of multiple JSONPatch operations
    // The SessionDelta should just become this once everything is streamlined
    public class SessionOpDelta
    {
        [JsonProperty("delta")]
        public IEnumerable<JsonPatchDocument> Operations { get; set; }
    }

    public struct EntityOperation
    {
        [JsonProperty("id")]
        public string EntityId { get; set; }

        [JsonProperty("patch")]
        public JsonPatchDocument Patch { get; set; }

        public EntityOperation(string entityId, JsonPatchDocument patch)
        {
            this.EntityId = entityId;
            this.Patch = patch;
        }
    }
}
