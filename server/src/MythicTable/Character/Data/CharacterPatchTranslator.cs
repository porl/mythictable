﻿using MythicTable.Util.JsonPatch;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MythicTable.Character.Data
{
    public class CharacterPatchTranslator
    {
        public CharacterPatchTranslator()
        {
        }

        public string JsonPath2MongoPath(string path)
        {
            string newPath = path.Substring(1);

            foreach (var root in new List<string>{ "Token", "Attributes", "Asset" })
            {
                newPath = new Regex("^" + root.ToLower()).Replace(newPath, root, 1);
            }
            newPath = newPath.Replace("/", ".");
            newPath = new Regex("\\.$").Replace(newPath, "", 1);
            return "Characters.$." + newPath;
        }

        public object Json2Mongo(object json)
        {
            if( json is JValue)
            {
                return (json as JValue).Value;
            }
            else if(json is JObject)
            {
                return (json as JObject).AsBson();
            }
            return json;
        }
    }
}