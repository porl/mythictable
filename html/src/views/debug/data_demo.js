const entities = [
    {
        id: 'plusButton',
        asset: { kind: 'image', src: '/static/assets/buttons/Buttons-plus.jpg' },
    },
    {
        id: 'minusButton',
        asset: { kind: 'image', src: '/static/assets/buttons/Buttons-minus.jpg' },
    },
    {
        id: 'undoButton',
        asset: { kind: 'image', src: '/static/assets/buttons/Buttons-undo.jpg' },
    },
    {
        id: 'redoButton',
        asset: { kind: 'image', src: '/static/assets/buttons/Buttons-redo.jpg' },
    },

    // Stages/scenes ----------------------------
    {
        id: 'debug',
        scene: { stage: '.' },
        stage: {
            grid: { type: 'square', size: 50 },
            bounds: {
                nw: { q: -1, r: -1 },
                se: { q: 29, r: 20 },
            },
            color: '#223344',
            elements: [
                {
                    id: 'background',
                    asset: 'stronghold_bg',
                    pos: { q: 0, r: 0, pa: '00' },
                },
            ],
        },
    },

    // Plain assets --------------------------
    {
        id: 'stronghold_bg',
        asset: { kind: 'image', src: '/static/assets/hideout.png' },
    },
];

const loader = async store => {
    await store.dispatch('gamestate/entities/load', entities);
};

export { loader, entities };
