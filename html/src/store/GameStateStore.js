// Module for representing gamestate in Vuex
// The base gamestate is the state of all objects in the game at a certain snapshot
// Every time one or multiple object states change through actions of the clients the gamestate changes
// Since it is a requirement to be able to undo an action, a list of changes (or deltas) is used to represent the current gamestate
// This allows for trivial undos by replaying all previous deltas since the game state

import EntityStore from './EntityStore';
import Vue from 'vue';
import jsonpatch from 'fast-json-patch';
import _ from 'lodash';
import ActionResolver from '@/core/resolution/ActionResolver';

// Invariant: gamestate = foldl(base, applyDeltas, deltas)
let GameStateStore = {
    namespaced: true,
    state: {
        base: {
            global: {},
            entities: {},
            ruleset: {},
        },
        deltas: [],
        undoneDeltas: [],
        global: {
            rollLog: [],
        },
        ruleset: {},
        selectedTokenId: '',
    },
    modules: {
        entities: EntityStore,
    },
    getters: {
        base: state => {
            return state.base;
        },
        ruleset: state => {
            return state.ruleset;
        },
        actions: state => {
            return state.ruleset.actions;
        },
        rollLog: state => {
            return state.global.rollLog;
        },
        selectedTokenId: state => {
            return state.selectedTokenId;
        },
    },
    mutations: {
        // Adds an arbitrary number of deltas to the deltas field
        // deltas can be both an array or a single value
        addDelta(state, delta) {
            if (delta == null || delta.length == 0) return;
            else if (Array.isArray(delta) && delta.length > 0) {
                delta = delta.filter(function(el) {
                    return el != null;
                });
            } else delta = [delta];

            state.deltas.push(delta);
        },
        resetGameState(state) {
            // Reset all deltas
            Vue.set(state, 'deltas', []);

            // Reset all entities that existed at base
            const baseEntities = _.cloneDeep(state.base.entities);
            const baseGlobal = _.cloneDeep(state.base.global);
            Object.assign(state.entities, baseEntities);
            Object.assign(state.global, baseGlobal);

            // Remove all entities that have been added to the base
            const removeAddedProps = function(dirty, base) {
                for (var prop in dirty) {
                    if (!base.hasOwnProperty(prop)) {
                        Vue.delete(dirty, prop);
                    }
                }
            };

            removeAddedProps(state.entities, baseEntities);
            removeAddedProps(state.global, baseGlobal);
        },
        resetUndoneDeltas(state) {
            state.undoneDeltas = [];
        },
        selectedTokenUpdate(state, tokenId) {
            state.selectedTokenId = tokenId;
        },
    },
    actions: {
        clear({ state }) {
            Vue.set(state, 'entities', {});
            Vue.set(state, 'global', { rollLog: [] });
        },
        setBase({ state }) {
            Vue.set(state.base, 'entities', _.cloneDeep(state.entities));
            Vue.set(state.base, 'global', _.cloneDeep(state.global));
            Vue.set(state.base, 'ruleset', _.cloneDeep(state.ruleset));
        },
        applyDelta({ commit, dispatch }, delta) {
            if (!Array.isArray(delta)) delta = [delta];

            delta.forEach(patch => {
                dispatch('patch', patch);
            });
            commit('addDelta', delta);
        },
        resolveAction({ rootState }, action) {
            let delta = [];
            if (action.id == 'undo') {
                rootState.live.director.submitUndo();
            } else if (action.id == 'redo') {
                rootState.live.director.submitRedo();
            } else {
                // TODO: #6, #17: Make a call to the server for the GM client to resolve the action in case this is the player client
                delta = ActionResolver.resolve(action);
                rootState.live.director.submitDelta(delta);
            }
        },
        undo({ commit, dispatch, state }) {
            let deltas = _.cloneDeep(state.deltas);
            if (deltas == null || deltas.length == 0) return;

            let undoneDelta = deltas.pop();
            state.undoneDeltas.push(undoneDelta);

            commit('resetGameState');

            deltas.forEach(delta => {
                dispatch('applyDelta', delta);
            });
        },
        redo({ dispatch, state }) {
            let delta = state.undoneDeltas.pop();
            if (!delta) return;

            dispatch('applyDelta', delta);
        },

        // Patch is a list of JSON patch operations as described in: http://jsonpatch.com/
        // IMPORTANT POINT TO NOTE: Vue cannot detect property addition or deletion.
        // What needs to be done is check if an operation is anything else than a "replace"
        // And depending on that, handle the Vue reactivity stuff when applying the patch.
        patch({ state }, patch) {
            if (!Array.isArray(patch)) patch = [patch];
            if (patch.length == 0) return;

            // Dry run the JSONpatch to allow for aborting
            // If this throws an error, the patch is not applied
            jsonpatch.applyPatch(state, patch, true, false).pop().newDocument;

            // Execute the patch with appropriate vue operations when needed
            patch.forEach(change => {
                const followPath = path => {
                    return path.reduce((acc, el) => {
                        return acc[el];
                    }, state);
                };

                const parsePath = pathString => {
                    const path = pathString.split('/');
                    path.shift(); // remove the first, empty element

                    if (path[0] != 'entities' && path[0] != 'global')
                        throw new Error('Attempted to touch protected property: ' + path[0]);

                    // Handle JSONPatch syntactic sugar
                    path.forEach((el, idx) => {
                        switch (el) {
                            case '-':
                                path[idx] = followPath(path.slice(0, idx)).length;
                                break;
                        }
                    });
                    return path;
                };

                const path = parsePath(change.path);

                switch (change.op) {
                    case 'replace':
                        jsonpatch.applyPatch(state, [change]);
                        break;
                    case 'add': {
                        let prop = path.pop();
                        if (Array.isArray(followPath(path))) {
                            followPath(path).splice(prop, 0, change.value); // Insert the value at the given index
                        } else {
                            Vue.set(followPath(path), prop, change.value);
                        }
                        break;
                    }
                    case 'remove': {
                        let prop = path.pop();

                        if (prop in state) throw new Error('Attempted to remove protected property: ' + prop);
                        Vue.delete(followPath(path), prop);
                        break;
                    }
                    case 'move': {
                        const from = parsePath(change.from);
                        const moved = _.cloneDeep(followPath(from));

                        const fromProp = from.pop();

                        if (fromProp in state) throw new Error('Attempted to remove protected property: ' + prop);
                        Vue.delete(followPath(from), fromProp);

                        let prop = path.pop();
                        Vue.set(followPath(path), prop, moved);
                        break;
                    }
                    case 'copy': {
                        const from = parsePath(change.from);
                        const copied = _.cloneDeep(followPath(from));

                        let prop = path.pop();
                        Vue.set(followPath(path), prop, copied);
                        break;
                    }
                    case 'test':
                        // This one is already handled by the dry run. It does not change anything to the state
                        break;
                }
            });
        },
    },
};

export default GameStateStore;
